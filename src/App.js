import { useEffect } from "react";
import "./App.css";

function App() {
  const options = {
    headers: {
      Authorization:
        "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjZEMThFM0I1NDVDQTdFQUQ0QUUyRjUyRDgzOThGODE1QzVBMkI1MTFSUzI1NiIsInR5cCI6ImF0K2p3dCIsIng1dCI6ImJSamp0VVhLZnExSzR2VXRnNWo0RmNXaXRSRSJ9.eyJuYmYiOjE2ODM3MTA3MjEsImV4cCI6MTY4NDkyMDMyMSwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo1MDAxIiwiYXVkIjpbImNvbS5zbWFydGFwcGhvbGRpbmcuc2FuYWRhay5hbmFseXRpY3MiLCJjb20uc21hcnRhcHBob2xkaW5nLnNhbmFkYWsuYXJlYXMiLCJjb20uc21hcnRhcHBob2xkaW5nLnNhbmFkYWsuY2hhdCIsImNvbS5zbWFydGFwcGhvbGRpbmcuc2FuYWRhay5jb250ZW50IiwiY29tLnNtYXJ0YXBwaG9sZGluZy5zYW5hZGFrLmZvbGxvdyIsImNvbS5zbWFydGFwcGhvbGRpbmcuc2FuYWRhay5pZGVudGl0eSIsImNvbS5zbWFydGFwcGhvbGRpbmcuc2FuYWRhay5saXN0aW5ncyIsImNvbS5zbWFydGFwcGhvbGRpbmcuc2FuYWRhay5tZXNzYWdlcyIsImNvbS5zbWFydGFwcGhvbGRpbmcuc2FuYWRhay5wZXJtaXNzaW9ucyIsImNvbS5zbWFydGFwcGhvbGRpbmcuc2FuYWRhay5wcm9maWxlcyIsImNvbS5zbWFydGFwcGhvbGRpbmcuc2FuYWRhay5wcm9qZWN0cyIsImNvbS5zbWFydGFwcGhvbGRpbmcuc2FuYWRhay5yZWFjdGlvbnMiLCJjb20uc21hcnRhcHBob2xkaW5nLnNhbmFkYWsuc2VhcmNoIiwiY29tLnNtYXJ0YXBwaG9sZGluZy5zYW5hZGFrLnN0b3JpZXMiXSwiY2xpZW50X2lkIjoiY29tLmxhcmtzdC5yaWxhLmFwcHMuMjM4OTQ3MjAyMzUyMy1pdndiZTVpd2VvYzdtcXdibjEiLCJzdWIiOiJkMThmMDEyZC0zZmY5LTQxN2YtYTNkYi03NTY3YTQ5YWYwZTciLCJhdXRoX3RpbWUiOjE2ODM3MTA3MjAsImlkcCI6ImxvY2FsIiwicm9sZSI6WyIxMDAxIiwiMjAyNyJdLCJjYW5BZGRTdG9yeSI6IkZhbHNlIiwidXNlclR5cGUiOiIxMDA0IiwiaXNQcm9maWxlQ29tcGxldGUiOiJUcnVlIiwianRpIjoiRUU1MTE3RUUxRDg1M0E2MjkzOTlBMzdCMUQxRjY3MTYiLCJpYXQiOjE2ODM3MTA3MjEsInNjb3BlIjpbImNvbS5zbWFydGFwcGhvbGRpbmcuc2FuYWRhay5hbmFseXRpY3MiLCJjb20uc21hcnRhcHBob2xkaW5nLnNhbmFkYWsuYXJlYXMiLCJjb20uc21hcnRhcHBob2xkaW5nLnNhbmFkYWsuY2hhdCIsImNvbS5zbWFydGFwcGhvbGRpbmcuc2FuYWRhay5jb250ZW50IiwiY29tLnNtYXJ0YXBwaG9sZGluZy5zYW5hZGFrLmZvbGxvdyIsImNvbS5zbWFydGFwcGhvbGRpbmcuc2FuYWRhay5pZGVudGl0eSIsImNvbS5zbWFydGFwcGhvbGRpbmcuc2FuYWRhay5saXN0aW5ncyIsImNvbS5zbWFydGFwcGhvbGRpbmcuc2FuYWRhay5tZXNzYWdlcyIsImNvbS5zbWFydGFwcGhvbGRpbmcuc2FuYWRhay5wZXJtaXNzaW9ucyIsImNvbS5zbWFydGFwcGhvbGRpbmcuc2FuYWRhay5wcm9maWxlcyIsImNvbS5zbWFydGFwcGhvbGRpbmcuc2FuYWRhay5wcm9qZWN0cyIsImNvbS5zbWFydGFwcGhvbGRpbmcuc2FuYWRhay5yZWFjdGlvbnMiLCJjb20uc21hcnRhcHBob2xkaW5nLnNhbmFkYWsuc2VhcmNoIiwiY29tLnNtYXJ0YXBwaG9sZGluZy5zYW5hZGFrLnN0b3JpZXMiLCJvZmZsaW5lX2FjY2VzcyJdLCJhbXIiOlsic2VjdXJpdHlfY29kZSJdfQ.DOZ9WV2YvdttT5KzM_m3eeS8OfBUoLvE9a1_yn7UmN7S_9TA-AuT7is5XUVLPvfcqKDfmx5yXQmLnStwFVwsgIyVGzIoo0G4Ywn0y7OmXvPa63FIeG2h_on-Uj2cgJUD6VUlBWARxbZVdggA8VjhVaPSxOmlyJrjJdwYgPUD5hkjL5zIHpBMVhrowI-8RW71bbtMgtft5PMk1wapHVgz4N5LKgyY8yWs5-OtilAidJ1Un4vvvvSyQZXFKspU2suwrnJPe36QEKjibzPXLJ_voFVXLo2IpFYclJKWZva8O_LOeu0weOgiFckhkQfbhxQWQMhPjH1Tlw4g6jnxqObfrWVPz-4hIoeWB2wbA37Ml8AJngJRLa0WpBZqbNfq2yzr_I3fH_aRW1YhoE2njsbzVVqeb8PemQDyWOVoql34y_HbNdjg383mp9z5C2bKHSP21lXq1Hz2zhCwzL-H5tmu9VKLhEJlGu6QeYnmwpgz8YA1yFXIzrGVPmAq9rz0AbCSsQKaVRlhdqgfOiz2xeZIqn8a7dyv-bJypzvhsI8udJtzFTXVwlTFFvVUA8gz4fxooywoLaYaltInhQRC2C3KVLJLAbX6l6GRMZzw6irn6wk-deOHqTlkQ732KJ0rDA8ZodRPeAM5smpV2Fas4i1UhhhkK6kTGp9WAC5d9VhzCFc",
    },
  };
  const fetchdata = () => {
    fetch(
      "https://api.smartappholding.com/dev/projects/v1/d5caee43-115f-4b80-acdb-d55c5f99f345",
      options
    )
      .then((resp) => resp.json())
      .then((json) => console.log("property data", JSON.stringify(json)));
    fetch(
      "https://api.smartappholding.com/dev/search/v1/Listings/0165802a-e037-4dd4-bea0-419115e1d042/similar",
      options
    )
      .then((resp) => resp.json())
      .then((json) => console.log("similar data", JSON.stringify(json)));
  };
  useEffect(() => {
    console.log(1111111);
    fetchdata();
  }, []);

  return (
    <div className="App">
      <header className="App-header">Test</header>
    </div>
  );
}

export default App;
